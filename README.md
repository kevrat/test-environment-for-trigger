# test-environment-for-trigger

## Description

A small showcase of support for the environment keyword in the trigger block for gitlab ci, introduced in release [16.4](https://about.gitlab.com/releases/2023/09/22/gitlab-16-4-released/).

## About

You can find more in my Telegram Channel: [@MaksimKlepikovBlog](https://t.me/MaksimKlepikov)
